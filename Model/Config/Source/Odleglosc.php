<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Callback\Model\Config\Source;

class Odleglosc implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '5', 'label' => __('5')], ['value' => '10', 'label' => __('10')], ['value' => '15', 'label' => __('15')], ['value' => '20', 'label' => __('20')], ['value' => '25', 'label' => __('25')], ['value' => '30', 'label' => __('30')], ['value' => '35', 'label' => __('35')], ['value' => '40', 'label' => __('40')], ['value' => '45', 'label' => __('45')], ['value' => '50', 'label' => __('50')]];
    }

    public function toArray()
    {
        return ['5' => __('5'), '10' => __('10'), '15' => __('15'), '20' => __('20'), '25' => __('25'), '30' => __('30'), '35' => __('35'), '40' => __('40'), '45' => __('45'), '50' => __('50')];
    }
}