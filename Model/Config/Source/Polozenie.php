<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Callback\Model\Config\Source;

class Polozenie implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'left', 'label' => __('Do lewej')], ['value' => 'right', 'label' => __('Do prawej')]];
    }

    public function toArray()
    {
        return ['left' => __('Do lewej'), 'right' => __('Do prawej')];
    }
}
