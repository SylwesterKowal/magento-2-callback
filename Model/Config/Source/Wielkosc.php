<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Callback\Model\Config\Source;

class Wielkosc implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '20', 'label' => __('20')], ['value' => '25', 'label' => __('25')], ['value' => '30', 'label' => __('30')], ['value' => '35', 'label' => __('35')], ['value' => '40', 'label' => __('40')], ['value' => '45', 'label' => __('45')], ['value' => '50', 'label' => __('50')], ['value' => '55', 'label' => __('55')], ['value' => '60', 'label' => __('60')], ['value' => '65', 'label' => __('65')], ['value' => '70', 'label' => __('70')], ['value' => '75', 'label' => __('75')], ['value' => '80', 'label' => __('80')], ['value' => '85', 'label' => __('85')], ['value' => '90', 'label' => __('90')], ['value' => '95', 'label' => __('95')], ['value' => '100', 'label' => __('100')]];
    }

    public function toArray()
    {
        return ['20' => __('20'), '25' => __('25'), '30' => __('30'), '35' => __('35'), '40' => __('40'), '45' => __('45'), '50' => __('50'), '55' => __('55'), '60' => __('60'), '65' => __('65'), '70' => __('70'), '75' => __('75'), '80' => __('80'), '85' => __('85'), '90' => __('90'), '95' => __('95'), '100' => __('100')];
    }
}