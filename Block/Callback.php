<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Callback\Block;

class Callback extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\Callback\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $data);
    }


    public function display()
    {
        if ( $this->dataHelper->getGeneralCfg('enable') ) {
            return true;
        } else {
            return false;
        }
    }

    public function animacja()
    {
        if ( $this->dataHelper->getGeneralCfg('animacja') ) {
            return true;
        } else {
            return false;
        }
    }

    public function phone()
    {
        if ( $phone = $this->dataHelper->getGeneralCfg('phone') ) {
            if ( $this->dataHelper->getGeneralCfg('whatsapp') ) {
                return 'https://api.whatsapp.com/send?phone=' . str_replace("+","",$phone);
            }else {
                return 'tel:' . $phone;
            }
        } else {
            return '#';
        }
    }
    public function wielkosc()
    {
        if ( $wielkosc = $this->dataHelper->getGeneralCfg('wielkosc') ) {
            return $wielkosc;
        } else {
            return 55;
        }
    }

    public function odleglosc()
    {
        if ( $wielkosc = $this->dataHelper->getGeneralCfg('odleglosc') ) {
            return $wielkosc;
        } else {
            return 20;
        }
    }

    public function polozenie()
    {
        if ( $polozenie = $this->dataHelper->getGeneralCfg('polozenie') ) {
            return $polozenie;
        } else {
            return 'right';
        }
    }
}

