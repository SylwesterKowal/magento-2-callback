define(['jquery'], function ($) {
    "use strict";
    return function callback() {

        (function ($) {
            function initData($el) {
                var _ARS_data = $el.data('_ARS_data');
                if (!_ARS_data) {
                    _ARS_data = {
                        rotateUnits: 'deg',
                        scale: 1,
                        rotate: 0
                    };
                    $el.data('_ARS_data', _ARS_data);
                }
                return _ARS_data;
            }

            function setTransform($el, data) {
                $el.css('transform', 'rotate(' + data.rotate + data.rotateUnits + ') scale(' + data.scale + ',' + data.scale + ')');
            }

            $.fn.rotate = function (val) {
                var $self = $(this), m, data = initData($self);
                if (typeof val == 'undefined') {
                    return data.rotate + data.rotateUnits;
                }
                m = val.toString().match(/^(-?\d+(\.\d+)?)(.+)?$/);
                if (m) {
                    if (m[3]) {
                        data.rotateUnits = m[3];
                    }
                    data.rotate = m[1];
                    setTransform($self, data);
                }
                return this;
            };

            // Note that scale is unitless.
            $.fn.scale = function (val) {
                var $self = $(this), data = initData($self);

                if (typeof val == 'undefined') {
                    return data.scale;
                }

                data.scale = val;

                setTransform($self, data);

                return this;
            };

            var curProxied = $.fx.prototype.cur;
            $.fx.prototype.cur = function () {
                if (this.prop == 'rotate') {
                    return parseFloat($(this.elem).rotate());

                } else if (this.prop == 'scale') {
                    return parseFloat($(this.elem).scale());
                }

                return curProxied.apply(this, arguments);
            };

            $.fx.step.rotate = function (fx) {
                var data = initData($(fx.elem));
                $(fx.elem).rotate(fx.now + data.rotateUnits);
            };

            $.fx.step.scale = function (fx) {
                $(fx.elem).scale(fx.now);
            };

            var animateProxied = $.fn.animate;
            $.fn.animate = function (prop) {
                if (typeof prop['rotate'] != 'undefined') {
                    var $self, data, m = prop['rotate'].toString().match(/^(([+-]=)?(-?\d+(\.\d+)?))(.+)?$/);
                    if (m && m[5]) {
                        $self = $(this);
                        data = initData($self);
                        data.rotateUnits = m[5];
                    }
                    prop['rotate'] = m[1];
                }
                return animateProxied.apply(this, arguments);
            };
        })(jQuery);

        $(function () {
            $(window).on('load', function waves0() {
                var width = screen.width;
                var right = width / 2;
                var img_width = $('#kowal-button-icon').attr('data-width');
                var img_height = $('#kowal-button-icon').attr('data-height');
                var pos_right = $('#kowal-button-icon').attr('data-right');
                var pos_bottom = $('#kowal-button-icon').attr('data-bottom');

                if (width >= 320 && width <= 960) {
                    $('#kowal-circle').css(prop(pos_right, pos_bottom,img_height,img_width,'0.2'));
                    $('#kowal-circle').animate(prop(pos_right - ((img_width * 1.5 - img_width) / 2), pos_bottom - ((img_height * 1.5 - img_height) / 2),img_height * 1.5,img_width * 1.5,'1'),
                        {
                            complete: waves_,
                            duration: 800
                        });
                } else {
                    $('#kowal-circle').css(prop(pos_right, pos_bottom,img_height,img_width,'0.2'));
                    $('#kowal-circle').animate(prop(pos_right - ((img_width * 1.5 - img_width) / 2), pos_bottom - ((img_height * 1.5 - img_height) / 2),img_height * 1.5,img_width * 1.5,'1'),
                        {
                            complete: waves_,
                            duration: 800
                        });
                }

                function waves_() {
                    var position = String($('#kowal-button-icon').attr('data-position'));
                    $('#kowal-waves').css(prop(pos_right - ((img_width * 1.5 - img_width) / 2) -2, pos_bottom - ((img_height * 1.5 - img_height) / 2) -2,img_height * 1.5,img_width * 1.5,'1'));
                    $('#kowal-circle').animate(prop(pos_right, pos_bottom,img_height,img_width,'0.2'),
                        {
                            duration: 1500
                        });
                    $('#kowal-waves').animate(prop(pos_right - ((img_width * 2 - img_width) / 2)-2, pos_bottom - ((img_height * 2 - img_height) / 2) -2,img_height * 2,img_width * 2,'0'),
                        {
                            complete: waves0,
                            duration: 1500
                        });
                }

                function prop(poloenie, bottom,height,width,opacity){
                    var position = String($('#kowal-button-icon').attr('data-position'));
                    if(position == 'left') {
                        return {
                            'left': poloenie + 'px',
                            'bottom': bottom + 'px',
                            'height': height + 'px',
                            'width': width + 'px',
                            'opacity': opacity
                        }
                    }else{
                        return {
                            'right': poloenie + 'px',
                            'bottom': bottom + 'px',
                            'height': height + 'px',
                            'width': width + 'px',
                            'opacity': opacity
                        }
                    }
                }


            });
//   Цвет/иконка при клике
            $('#kowal-button-icon').on('mouseenter', function () {
                $('#kowal-waves').css({'border-color': '#b2ebf2'});
                $('#kowal-circle').css({'background-color': '#b2ebf2'});
            });
            var rotation = function () {
                if (x > 0) {
                    var rotate3 = function () {
                        $('#kowal-button-icon').animate({rotate: "0deg"},
                            {
                                complete: rotation,
                                duration: 50
                            });
                    }
                    var rotate2 = function () {
                        $('#kowal-button-icon').animate({rotate: "-5deg"},
                            {
                                complete: rotation,
                                duration: 100
                            });
                    }
                    var rotate1 = function () {
                        $('#kowal-button-icon').animate({rotate: "10deg"},
                            {
                                complete: rotate2,
                                duration: 200
                            });
                    }
                    $('#kowal-button-icon').animate({rotate: "-20deg"},
                        {
                            complete: rotate1,
                            duration: 300
                        });

                } else {
                    return 0;
                }
            }, x;
            x = 1;
            rotation();
            //$('#kowal-button-icon').on('mouseenter', rotation);
            $('#kowal-button-icon').on('mouseleave', function () {
                $('#waves').css({'border-color': '#44f59a'});
                $('#circle').css({'background-color': '#8dffc4'});
            });
        });
    }
});